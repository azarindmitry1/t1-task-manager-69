package ru.t1.azarin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.azarin.tm.component.Bootstrap;
import ru.t1.azarin.tm.configuration.ClientConfiguration;

import java.sql.SQLException;

public final class ClientApplication {

    public static void main(@Nullable final String[] args) throws SQLException {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
