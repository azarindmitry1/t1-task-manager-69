<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1 style="text-align:center">TASK LIST</h1>
<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th nowrap="nowrap" align="center">ID</th>
        <th nowrap="nowrap" align="center">NAME</th>
        <th nowrap="nowrap" align="center">DESCRIPTION</th>
        <th nowrap="nowrap" align="center">PROJECT</th>
        <th nowrap="nowrap" align="center">STATUS</th>
        <th nowrap="nowrap" align="center">DATE START</th>
        <th nowrap="nowrap" align="center">DATE FINISH</th>
        <th nowrap="nowrap" align="center">UPDATE</th>
        <th nowrap="nowrap" align="center">DELETE</th>
    </tr>

	<c:forEach var="task" items="${tasks}">
	    <tr>
            <td nowrap="nowrap" align="center">
                <c:out value="${task.id}" />
            </td>
            <td nowrap="nowrap" align="center">
                <c:out value="${task.name}" />
            </td>
            <td nowrap="nowrap" align="center">
                <c:out value="${task.description}" />
            </td>
            <td nowrap="nowrap" align="center">
                <c:out value="${projectDtoService.findById(task.userId, task.projectId).name}" />
            </td>
            <td nowrap="nowrap" align="center">
                <c:out value="${task.status.displayName}" />
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateStart}" />
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateFinish}" />
            </td>
            <td nowrap="nowrap" align="center">
                <a href="/task/update/${task.id}">UPDATE</a>
            </td>
            <td nowrap="nowrap" align="center">
                <a href="/task/delete/${task.id}">DELETE</a>
            </td>
	    </tr>
	</c:forEach>
</table>

<form action="/task/create" method="POST" style="margin-top: 20px">
    <button>CREATE</button>
</form>

<jsp:include page="../include/_footer.jsp"/>