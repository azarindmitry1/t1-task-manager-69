<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1 style="text-align:center">PROJECT LIST</h1>
<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th nowrap="nowrap" align="center">ID</th>
        <th nowrap="nowrap" align="center">NAME</th>
        <th nowrap="nowrap" align="center">DESCRIPTION</th>
        <th nowrap="nowrap" align="center">STATUS</th>
        <th nowrap="nowrap" align="center">DATE START</th>
        <th nowrap="nowrap" align="center">DATE FINISH</th>
        <th nowrap="nowrap" align="center">UPDATE</th>
        <th nowrap="nowrap" align="center">DELETE</th>
    </tr>

	<c:forEach var="project" items="${projects}">
	    <tr>
            <td nowrap="nowrap" align="center">
                <c:out value="${project.id}" />
            </td>
            <td nowrap="nowrap" align="center">
                <c:out value="${project.name}" />
            </td>
            <td nowrap="nowrap" align="center">
                <c:out value="${project.description}" />
            </td>
            <td nowrap="nowrap" align="center">
                <c:out value="${project.status.displayName}" />
            </td>
            <td nowrap="nowrap" align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateStart}" />
            </td>
            <td nowrap="nowrap" align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateFinish}" />
            </td>
            <td nowrap="nowrap" align="center">
                <a href="/project/update/${project.id}">EDIT</a>
            </td>
            <td nowrap="nowrap" align="center">
                <a href="/project/delete/${project.id}">DELETE</a>
            </td>
	    </tr>
	</c:forEach>
</table>

<form action="/project/create" method="POST" style="margin-top: 20px">
    <button>CREATE</button>
</form>

<jsp:include page="../include/_footer.jsp"/>