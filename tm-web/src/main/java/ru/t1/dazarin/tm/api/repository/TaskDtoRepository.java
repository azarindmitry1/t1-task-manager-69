package ru.t1.dazarin.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.dazarin.tm.model.dto.TaskDto;

import java.util.List;

@Repository
public interface TaskDtoRepository extends JpaRepository<TaskDto, String> {

    TaskDto findByUserIdAndId(String userId, String id);

    List<TaskDto> findAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    void deleteByUserId(String userId);

}
