package ru.t1.dazarin.tm.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import ru.t1.dazarin.tm.model.dto.Message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class ServiceAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    @SneakyThrows
    public void commence(
            @NotNull final HttpServletRequest request,
            @NotNull final HttpServletResponse response,
            @NotNull final AuthenticationException authException
    ) {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        @NotNull final PrintWriter writer = response.getWriter();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String value = authException.getMessage();

        @NotNull final Message message = new Message(value);
        writer.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(message));
    }


}
