package ru.t1.dazarin.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dazarin.tm.model.dto.TaskDto;
import ru.t1.dazarin.tm.service.dto.TaskDtoService;
import ru.t1.dazarin.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.t1.dazarin.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    private final TaskDtoService taskDtoService;

    @Override
    @WebMethod
    @PostMapping("/create")
    public TaskDto create() {
        return taskDtoService.create(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping(value = "/findAll")
    public List<TaskDto> findAll() {
        return taskDtoService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping(value = "/findById/{id}")
    public TaskDto findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") final String id
    ) {
        return taskDtoService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @DeleteMapping(value = "/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") final String id
    ) {
        taskDtoService.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @DeleteMapping(value = "/deleteAll")
    public void deleteAll() {
        taskDtoService.deleteAll(UserUtil.getUserId());
    }

    @Override
    public TaskDto update(
            @WebParam(name = "taskDto", partName = "taskDto")
            @NotNull @RequestBody final TaskDto taskDto
    ) {
        return taskDtoService.save(UserUtil.getUserId(), taskDto);
    }

}
