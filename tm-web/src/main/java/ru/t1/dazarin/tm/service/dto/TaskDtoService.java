package ru.t1.dazarin.tm.service.dto;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dazarin.tm.api.repository.TaskDtoRepository;
import ru.t1.dazarin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dazarin.tm.model.dto.TaskDto;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskDtoService {

    private final TaskDtoRepository taskDtoRepository;

    public List<TaskDto> findAll(@NotNull final String userId) {
        return taskDtoRepository.findAllByUserId(userId);
    }

    @Nullable
    public TaskDto findById(@NotNull final String userId, @NotNull final String id) {
        return taskDtoRepository.findByUserIdAndId(userId, id);
    }

    public TaskDto create(@NotNull final String userId) {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setName("Task Name " + System.currentTimeMillis());
        taskDto.setDescription("Task Description");
        taskDto.setUserId(userId);
        return taskDtoRepository.saveAndFlush(taskDto);
    }

    public void deleteById(@NotNull final String userId, @NotNull final String taskDtoId) {
        if (!taskDtoRepository.existsById(taskDtoId)) throw new TaskNotFoundException();
        taskDtoRepository.deleteByUserIdAndId(userId, taskDtoId);
    }

    public void deleteAll(@NotNull final String userId) {
        taskDtoRepository.deleteByUserId(userId);
    }

    public TaskDto save(@NotNull final String userId, @NotNull final TaskDto taskDto) {
        taskDto.setUserId(userId);
        return taskDtoRepository.save(taskDto);
    }

}
