package ru.t1.dazarin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1.dazarin.tm.model.dto.Result;
import ru.t1.dazarin.tm.model.dto.UserDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface IAuthEndpoint {

    @WebMethod
    @PostMapping(value = "/login")
    Result login(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password
    );

    @WebMethod
    @GetMapping(value = "/profile")
    UserDto profile();

    @WebMethod
    @PostMapping(value = "/logout")
    Result logout();

}
