package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.service.dto.*;
import ru.t1.azarin.tm.api.service.model.*;

public interface IServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectDtoService getProjectServiceDTO();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IProjectTaskDtoService getProjectTaskServiceDTO();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    ITaskDtoService getTaskServiceDTO();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IUserDtoService getUserServiceDTO();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ISessionDtoService getSessionServiceDTO();

}
